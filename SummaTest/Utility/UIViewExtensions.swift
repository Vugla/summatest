//
//  UIViewExtensions.swift
//  mtelBA
//
//  Created by Predrag Samardzic on 3/23/18.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import UIKit

//  MARK: - Modals
extension UIView {
    
    func showActivityIndicator(_ style: UIActivityIndicatorViewStyle = .gray, atBottom: Bool = false) {
        hideActivityIndicator()
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: style)
        activityIndicator.tag = 7777
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(activityIndicator)
        bringSubview(toFront: activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        activityIndicator.heightAnchor.constraint(equalToConstant: 25).isActive = true
        activityIndicator.widthAnchor.constraint(equalToConstant: 25).isActive = true
        bringSubview(toFront: activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        viewWithTag(7777)?.removeFromSuperview()
    }
}
