//
//  Functions.swift
//  SummaTest
//
//  Created by Predrag Samardzic on 14/09/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import Foundation

//  MARK: - isNilOrEmpty()
protocol _CollectionOrStringish {
    var isEmpty: Bool { get }
}

extension String: _CollectionOrStringish {}
extension Array: _CollectionOrStringish {}
extension Dictionary: _CollectionOrStringish {}
extension Set: _CollectionOrStringish {}

extension Optional where Wrapped: _CollectionOrStringish {
    var isNilOrEmpty: Bool {
        switch self {
        case let .some(value): return value.isEmpty
        default: return true
        }
    }
}
// end: isNilOrEmpty()
