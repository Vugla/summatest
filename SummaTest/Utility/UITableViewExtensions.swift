//
//  UITableViewExtensions.swift
//  mtelBA
//
//  Created by Predrag Samardzic on 3/23/18.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import UIKit

extension UITableView {
    
    func setupRefreshControl(refreshControl: UIRefreshControl, activityIndicator: UIActivityIndicatorView, selector: Selector, sender: UIViewController) {
        let tableViewController = UITableViewController()
        
        tableViewController.tableView = self
        refreshControl.tintColor = UIColor.clear
        activityIndicator.clipsToBounds = true
        refreshControl.addSubview(activityIndicator)
        refreshControl.addTarget(sender, action: selector, for: .valueChanged)
        tableViewController.refreshControl = refreshControl
    }
}
