//
//  GroupMember.swift
//  SummaTest
//
//  Created by Predrag Samardzic on 14/09/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import Foundation

struct GroupMember: Codable {
    let userId: String
    let groupId: String
    
    enum CodingKeys : String, CodingKey {
        case userId = "user_id"
        case groupId = "group_id"
    }
}
