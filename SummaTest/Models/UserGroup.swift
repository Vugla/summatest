//
//  UserGroup.swift
//  SummaTest
//
//  Created by Predrag Samardzic on 14/09/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import Foundation

enum GroupType: String, Codable {
    case online, system, favorite
}

struct UsersGroup: Codable {
    let ownerId: String?
    let order: Int
    let type: GroupType
    let name: String?
    let groupId: String
    
    enum CodingKeys : String, CodingKey {
        case ownerId = "owner_id"
        case order = "order"
        case type = "type"
        case name = "name"
        case groupId = "group_id"
    }
}
