//
//  User.swift
//  SummaTest
//
//  Created by Predrag Samardzic on 14/09/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import Foundation

enum PresenceStatus: String, Codable {
    case online
    case offline
    case onDevice = "on-device"
    
}

struct User: Codable {
    let avatar: String?
    let presenceMessage: String?
    let displayName: String?
    let jobTitle: String?
    let status: PresenceStatus
    let email: String?
    let id: String
    let identity: String?
    let ext: String?
    
    enum CodingKeys : String, CodingKey {
        case avatar = "avatar"
        case presenceMessage = "presence_message"
        case displayName = "displayname"
        case jobTitle = "job_title"
        case status = "presence_code"
        case email = "email"
        case id = "user_id"
        case identity = "identity"
        case ext = "extension"
    }
}
