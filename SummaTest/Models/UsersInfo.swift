//
//  UsersInfo.swift
//  SummaTest
//
//  Created by Predrag Samardzic on 14/09/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import Foundation

struct UsersInfo: Codable {
    let groups: [String: UsersGroup]?
    let users: [String: User]?
    let members: [String: GroupMember]?
    
    enum CodingKeys : String, CodingKey {
        case groups = "groups"
        case users = "users"
        case members = "members"
    }
}
