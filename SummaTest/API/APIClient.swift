//
//  APIClient.swift
//  N1 info
//
//  Created by Predrag Samardzic on 8/23/18.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import Foundation

public enum RequestType: String {
    case GET, POST
}

protocol APIRequest {
    associatedtype argType: Decodable
    var method: RequestType { get }
    var path: String { get }
    var parameters: [String : String] { get }
}

extension APIRequest {
    func request(with baseURL: URL) -> URLRequest {
        if var components = URLComponents(url: baseURL.appendingPathComponent(path), resolvingAgainstBaseURL: false) {
            if components.queryItems == nil || components.queryItems!.count == 0 {
                components.queryItems = parameters.count > 0 ? parameters.map {
                    URLQueryItem(name: String($0), value: String($1))
                    } : nil
            } else {
                components.queryItems = components.queryItems! + parameters.map {
                    URLQueryItem(name: String($0), value: String($1))
                }
            }
            guard let url = components.url else {
                fatalError("Could not get url")
            }
            
            var request = URLRequest(url: url)
            request.httpMethod = method.rawValue
            return request
        }
        fatalError("Unable to create URL components")
    }
}

protocol APIClient {
    func send<T: APIRequest>(apiRequest: T, completion: @escaping (T.argType?, Error?) -> Void)
}

class SummaAPIClient: APIClient {
    func send<T: APIRequest>(apiRequest: T, completion: @escaping (T.argType?, Error?) -> Void) {
        let request = apiRequest.request(with: self.baseURL)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(nil, error)
                } else if let model = try? JSONDecoder().decode(T.argType.self, from: data ?? Data()) {
                    completion(model, error)
                } else {
                    completion(nil, NSError(domain: self.domainName, code: self.defaultErrorCode, userInfo:[NSLocalizedDescriptionKey:"Parsing Error."]))
                }
            }
        }
        task.resume()
    }
    
    
    let domainName = "Summa"
    let defaultErrorCode = 600
    
    private var baseURL: URL
    
    init(baseUrl: URL) {
        self.baseURL = baseUrl
    }
}
