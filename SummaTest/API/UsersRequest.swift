//
//  UsersRequest.swift
//  SummaTest
//
//  Created by Predrag Samardzic on 14/09/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import Foundation

class UsersRequest: APIRequest {
    typealias argType = UsersInfo
    var method = RequestType.GET
    var path = "summa_roster.json"
    var parameters = [String: String]()
}
