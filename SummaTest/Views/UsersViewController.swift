//
//  UsersViewController.swift
//  SummaTest
//
//  Created by Predrag Samardzic on 14/09/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import UIKit

class UsersViewController: UIViewController {
    
    let apiClient = SummaAPIClient(baseUrl: URL(string: "http://uc-dev.voiceworks.com:8083")!)
    let savedDataKey = "datasource"
    let favoritesKey = "favorites"
    
    @IBOutlet weak var tableView: UITableView!
    
    private struct Group: Codable {
        var name: String?
        var groupId: String?
        var type: GroupType
        var users = [User]()
    }
    private var datasource = [Group]() {
        didSet {
            tableView?.reloadData()
        }
    }
    private var favorites = [String]()
//    private var allUsers = [User]()
    private var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        getSavedData()
        loadData()
    }
    
    @objc private func loadData() {
        if !refreshControl.isRefreshing {
            view.showActivityIndicator()
        }
        view.hideError()
        apiClient.send(apiRequest: UsersRequest()) { [weak self](info, error) in
            guard let `self` = self else { return }
            self.view.hideActivityIndicator()
            self.refreshControl.endRefreshing()
            guard let info = info, error == nil else {
                self.view.showError(type: .server, delegate: self)
                return
            }
//            self.allUsers = info.users?.values.map({$0}) ?? []
            self.populateDatasource(userInfo: info)
            self.saveData()
        }
    }
    
    private func populateDatasource(userInfo: UsersInfo) {
        
        //populate datasource with groups (got name, type and id here, will populate users later)
        if let datasource = userInfo.groups?.values
            .sorted(by: {$0.order < $1.order})
            .map({Group(name: $0.name.isNilOrEmpty ? $0.type.rawValue : $0.name, groupId: $0.groupId, type: $0.type, users: [])}){
            self.datasource = datasource
            //populate members for each group
            for (index, group) in datasource.enumerated() {
                if group.type == .online {
                    self.datasource[index].users = userInfo.users?.values.filter({$0.status == .online || $0.status == .onDevice}) ?? []
                }
                    //get all user ids for members of group
                else if let userIds = userInfo.members?.values.filter({$0.groupId == group.groupId}).map({$0.userId}) {
                    //find users appertaining to group
                    //FIRST WAY - assuming there was no "hacky" naming convention in json
                    //                    self.datasource[index].users = userInfo.users?.values.filter({userIds.contains($0.id)}) ?? []
                    //SECOND WAY, using keys (saves going through all users, instead accesses user directly
                    var users = [User]()
                    for id in userIds {
                        if let user = userInfo.users?[id] {
                            users.append(user)
                        }
                    }
                    self.datasource[index].users = users
                    
                    if group.type == .favorite {
                        self.favorites = users.map({$0.id})
                    }
                }
            }
        }
    }
    
    private func setupTableView() {
        tableView.tableFooterView = UIView()
        refreshControl.addTarget(self, action: #selector(loadData), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    private func saveData() {
        if let encoded = try? JSONEncoder().encode(datasource) {
            UserDefaults.standard.set(encoded, forKey: savedDataKey)
        }
    }
    
    private func getSavedData() {
        if let decoded = UserDefaults.standard.object(forKey: savedDataKey) as? Data {
            datasource = (try? JSONDecoder().decode([Group].self, from: decoded)) ?? []
        }
    }
    
    private func saveFavorite(user: User) {
        favorites.append(user.id)
        if let groupIndex = datasource.index(where: {$0.type == .favorite}) {
            datasource[groupIndex].users.append(user)
            tableView.reloadData()
        }
    }

    private func removeFavorite(user: User, groupIndex: Int) {
        if let index = favorites.index(where: {$0 == user.id}) {
            favorites.remove(at: index)
        }
        if let userIndex = datasource[groupIndex].users.index(where: {$0.id == user.id}) {
            datasource[groupIndex].users.remove(at: userIndex)
        }
        if let favoritesGroupIndex = datasource.index(where: {$0.type == .favorite}) {
            if let userIndex = datasource[favoritesGroupIndex].users.index(where: {$0.id == user.id}) {
                datasource[favoritesGroupIndex].users.remove(at: userIndex)
            }
        }
        tableView.reloadData()
    }
}

extension UsersViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return datasource.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource[section].users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = datasource[indexPath.section].users[indexPath.row]
        return UserTableViewCell.cell(for: user, isFavorite: favorites.contains(user.id), delegate: self, in: tableView, at: indexPath)
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return datasource[section].name
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension UsersViewController: FavoritesDelegate {
    func setFavorite(cell: UITableViewCell, favorite: Bool) {
        if let indexPath = tableView.indexPath(for: cell) {
            let user = datasource[indexPath.section].users[indexPath.row]
            if favorite {
                saveFavorite(user: user)
            } else {
                removeFavorite(user: user, groupIndex: indexPath.section)
            }
        }
    }
}

extension UsersViewController: ErrorViewDelegate {
    func tryAgain() {
        loadData()
    }
}
