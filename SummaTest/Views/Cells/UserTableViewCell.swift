//
//  UserTableViewCell.swift
//  SummaTest
//
//  Created by Predrag Samardzic on 14/09/2018.
//  Copyright © 2018 Predrag Samardzic. All rights reserved.
//

import UIKit

protocol FavoritesDelegate: class {
    func setFavorite(cell: UITableViewCell, favorite: Bool)
}

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var extensionLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    weak var delegate: FavoritesDelegate?
    
    static func cell(for user:User, isFavorite: Bool, delegate: FavoritesDelegate, in tableView:UITableView, at indexPath:IndexPath) -> UserTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
        
        cell.delegate = delegate
        cell.favoriteButton.isSelected = isFavorite
        cell.nameLabel.text = user.displayName
        if let index = user.ext?.range(of: "@")?.lowerBound, let firstPart = user.ext?[..<index] {
            cell.extensionLabel.text = String(firstPart)
        }
        cell.statusLabel.text = user.presenceMessage
        cell.statusLabel.textColor = user.status == .offline ? .red : .green
        
        return cell
    }
    
    @IBAction func tapOnFavoriteButton(_ sender: UIButton) {
        delegate?.setFavorite(cell: self, favorite: !sender.isSelected)
    }
}
